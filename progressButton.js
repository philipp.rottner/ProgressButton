(function (window, $) {
	/**
	 * Object bootstrap
	 * @param {jQuery selected DOM object} button
	 * @param {array} options
	 */
	var ProgressButton = function (button, options) {
		this.button = button;
		this.$button = $(button);
		this.options = options;
	};

	ProgressButton.prototype = {
		/**
		 * Default options: progress button holds at 80% progress
		 * also each iteration with max 10% progress is 150ms after the previous
		 * the button is reset to its original status 3500ms after the completing callback
		 */
		defaults: {
			holdingProgressAt: 0.8,
			refreshInterval: 150,
			statusTime: 3500,
		},
		/**
		 * Initialisation: original button is placed inside content span
		 * additionally a hidden progress span is added
		 */
		init: function () {
			this.config = $.extend({}, this.defaults, this.options);
			this.button.attr('data-style', 'shrink');
			this.button.attr('data-horizontal', '');
			var contentNewButton = '<span class="content">' + this.button.html() + '</span>';
			var progressNewButton = '<span class="progress"><span class="progress-inner"></span></span>';
			this.button.html(contentNewButton);
			this.button.append(progressNewButton);
			this.progress = this.button.find('span.progress-inner');
			this.button.removeAttr('disabled');
			return this;
		},
		/**
		 * The progress of the button is started
		 * by random number creation with little time deltas it is ensured that the user is
		 * under the impression that the progress bar shows a real,
		 * because non linear status indication; the progress stops after a defined percentage,
		 * waiting for the final callback
		 */
		startProgress: function () {
			this.button.attr('disabled', true);
			this.progress.removeClass('notransition');
			this.button.addClass('state-loading');
			var progress = 0;
			var self = this;
			this.runningIntervall = setInterval(function () {
				if (progress < self.config.holdingProgressAt) {
					progress = Math.min(progress + Math.random() * 0.1, 1);
					self.setProgress(progress);
				} else {
					clearInterval(this.runningIntervall);
				}

			}, this.config.refreshInterval);
		},
		/**
		 * Sets the progress to a certain percantage
		 * @param {integer} progress
		 */
		setProgress: function (progress) {
			this.progress.width(100 * progress + '%');
		},
		/**
		 * Sets the progress to zero and the opacity back to one after resetting
		 */
		resetProgress: function () {
			this.progress.css({
				opacity: 0,
			});
			this.progress.width(0);
			this.progress.css({
				opacity: 1,
			});
		},
		/**
		 * Sets the button into a feedback status indicating the success of the button action
		 * for a defined time; error status is shown when status is negative, else success
		 * @param {integer} status
		 */
		setButtonToFeedback: function (status) {
			this.button.removeClass('state-loading');
			var self = this;
			if (typeof status === 'number') {
				var statusClass = status >= 0 ? 'state-success' : 'state-error';
				this.button.addClass(statusClass);
				setTimeout(function () {
					self.resetButton();
				}, self.config.statusTime);
			} else {
				this.resetButton();
			}
		},
		/**
		 * Resets the button to its original state
		 */
		resetButton: function () {
			this.button.removeClass('state-success');
			this.button.removeClass('state-error');
			this.button.removeAttr('disabled');
		},
		/**
		 * Finished the progress to a 100%; shows the success of the button action afterwards
		 * @param {integer} status
		 */
		stopProgress: function (status) {
			clearInterval(this.runningIntervall);
			this.setProgress(1);
			this.resetProgress();
			this.setButtonToFeedback(status);
		}
	}

	/**
	 * sets new progress button instances to the default options
	 */
	ProgressButton.defaults = ProgressButton.prototype.defaults;

	/**
	 * if custom parameters were used on object call, those are used instead
	 * @param   {array} options
	 * @returns {object instance}
	 */
	$.fn.progressButton = function (options) {
		return this.each(function () {
			new ProgressButton(this, options).init();
		});
	};

	window.ProgressButton = ProgressButton;
})(window, jQuery);
